/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    tim.h
  * @brief   This file contains all the function prototypes for
  *          the tim.c file
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TIM_H__
#define __TIM_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

void MX_TIM1_Init(void);
void MX_TIM14_Init(void);

/* USER CODE BEGIN Prototypes */
// Call from Application
uint32_t TIM_GetPWMPulseWidthMax();
void TIM_SetPWMPulseWidth(uint32_t u_pulse_width, uint32_t v_pulse_width, uint32_t w_pulse_width);
void TIM_SetEnable(uint8_t u_enable, uint8_t v_enable, uint8_t w_enable);
uint32_t* TIM_RefCntUpPulseWidth();
uint32_t* TIM_RefCntDownPulseWidth();
uint32_t TIM_GetCount();

void TIM_ResetStopWatch();
uint32_t TIM_GetStopWatchCount();
// Call from LL
uint8_t TIM1_IsReachedCntOverflow();
void TIM1_UpdateCntUpPulseWidth();
void TIM1_UpdateCntDownPulseWidth();

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif

#endif /* __TIM_H__ */

