/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    i2c.c
  * @brief   This file provides code for the configuration
  *          of the I2C instances.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "i2c.h"

/* USER CODE BEGIN 0 */
#include "string.h"

#define I2C1_TX_BUFFER_SIZE 8
#define I2C1_RX_BUFFER_SIZE 8

static uint8_t i2c1_tx_buffer_[I2C1_TX_BUFFER_SIZE];
static uint8_t i2c1_rx_buffer_[I2C1_RX_BUFFER_SIZE];

static uint32_t slave_address_;
static uint32_t receive_size_;
static uint8_t receive_flag_;
/* USER CODE END 0 */

/* I2C1 init function */
void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  LL_I2C_InitTypeDef I2C_InitStruct = {0};

  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
  /**I2C1 GPIO Configuration
  PB8   ------> I2C1_SCL
  PB9   ------> I2C1_SDA
  */
  GPIO_InitStruct.Pin = LL_GPIO_PIN_8;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_1;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_9;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_1;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* Peripheral clock enable */
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_I2C1);

  /* I2C1 DMA Init */

  /* I2C1_TX Init */
  LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_2, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);

  LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_2, LL_DMA_PRIORITY_LOW);

  LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_2, LL_DMA_MODE_NORMAL);

  LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_2, LL_DMA_PERIPH_NOINCREMENT);

  LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_2, LL_DMA_MEMORY_INCREMENT);

  LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_2, LL_DMA_PDATAALIGN_BYTE);

  LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_2, LL_DMA_MDATAALIGN_BYTE);

  /* I2C1_RX Init */
  LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_3, LL_DMA_DIRECTION_PERIPH_TO_MEMORY);

  LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PRIORITY_LOW);

  LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MODE_NORMAL);

  LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PERIPH_NOINCREMENT);

  LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MEMORY_INCREMENT);

  LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PDATAALIGN_BYTE);

  LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MDATAALIGN_BYTE);

  /* I2C1 interrupt Init */
  NVIC_SetPriority(I2C1_IRQn, 0);
  NVIC_EnableIRQ(I2C1_IRQn);

  /* USER CODE BEGIN I2C1_Init 1 */
  uint8_t i = 0;
  // TX DMA
  for (i = 0; i < I2C1_TX_BUFFER_SIZE; i++) {
    i2c1_tx_buffer_[i] = 0;
  }
  LL_DMA_ConfigAddresses(DMA1, LL_DMA_CHANNEL_2, (uint32_t)&i2c1_tx_buffer_, LL_I2C_DMA_GetRegAddr(I2C1, LL_I2C_DMA_REG_DATA_TRANSMIT), \
                         LL_DMA_GetDataTransferDirection(DMA1, LL_DMA_CHANNEL_2));
  // RX DMA
  for (i = 0; i < I2C1_TX_BUFFER_SIZE; i++) {
    i2c1_rx_buffer_[i] = 0;
  }
  LL_DMA_ConfigAddresses(DMA1, LL_DMA_CHANNEL_3, LL_I2C_DMA_GetRegAddr(I2C1, LL_I2C_DMA_REG_DATA_RECEIVE), (uint32_t)&i2c1_rx_buffer_,\
                         LL_DMA_GetDataTransferDirection(DMA1, LL_DMA_CHANNEL_3));
  /* USER CODE END I2C1_Init 1 */

  /** I2C Initialization
  */
  LL_I2C_DisableOwnAddress2(I2C1);
  LL_I2C_DisableGeneralCall(I2C1);
  LL_I2C_EnableClockStretching(I2C1);
  I2C_InitStruct.PeripheralMode = LL_I2C_MODE_I2C;
  I2C_InitStruct.Timing = 0x20000209;
  I2C_InitStruct.AnalogFilter = LL_I2C_ANALOGFILTER_ENABLE;
  I2C_InitStruct.DigitalFilter = 0;
  I2C_InitStruct.OwnAddress1 = 0;
  I2C_InitStruct.TypeAcknowledge = LL_I2C_ACK;
  I2C_InitStruct.OwnAddrSize = LL_I2C_OWNADDRESS1_7BIT;
  LL_I2C_Init(I2C1, &I2C_InitStruct);
  LL_I2C_EnableAutoEndMode(I2C1);
  LL_I2C_SetOwnAddress2(I2C1, 0, LL_I2C_OWNADDRESS2_NOMASK);

  /** I2C Fast mode Plus enable
  */
  LL_SYSCFG_EnableFastModePlus(LL_SYSCFG_I2C_FASTMODEPLUS_I2C1);
  /* USER CODE BEGIN I2C1_Init 2 */
  LL_I2C_EnableDMAReq_TX(I2C1);
  LL_I2C_EnableDMAReq_RX(I2C1);
  /* USER CODE END I2C1_Init 2 */

}

/* USER CODE BEGIN 1 */
void I2C_ReceiveAfterTransmit(uint32_t slave_address, uint8_t transmit_size, uint8_t receive_size) {
  // set data
  slave_address_ = slave_address;
  receive_size_ = receive_size;
  receive_flag_ = 0;
  // wait until bus is empty
  while(LL_I2C_IsActiveFlag_BUSY(I2C1));
  // transmit data via dma
  LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_2, transmit_size);
  LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_2);
  // start transmit
  LL_I2C_HandleTransfer(I2C1, slave_address, LL_I2C_ADDRSLAVE_7BIT, transmit_size, LL_I2C_MODE_SOFTEND, LL_I2C_GENERATE_START_WRITE);
  // wait transmit complete interrupt
  LL_I2C_EnableIT_TC(I2C1);
}

uint8_t* I2C_RefTxBuffer() {
  return i2c1_tx_buffer_;
}

uint8_t* I2C_RefRxBuffer() {
  return i2c1_rx_buffer_;
}

uint8_t* I2C_RefReceiveFlag() {
  return &receive_flag_;
}

void I2C1_TransmitCompleteCallback() {
  // disable transmit dma after transmit
  LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_2);
  // disable transmit complete interrupt
  LL_I2C_DisableIT_TC(I2C1);
  // receive data via dma
  LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_3, receive_size_);
  LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_3);
  // start read
  LL_I2C_HandleTransfer(I2C1, slave_address_ | 0x01, LL_I2C_ADDRSLAVE_7BIT, receive_size_, LL_I2C_MODE_AUTOEND, LL_I2C_GENERATE_START_READ);
  // wait stop condition interrupt
  LL_I2C_EnableIT_STOP(I2C1);
}

void I2C1_StopConditionDetectCallback() {
  // disable receive dma after receive
  LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_3);
  // disable stop condition interrupt
  LL_I2C_DisableIT_STOP(I2C1);
  // set update rx buffer flag
  receive_flag_ = 1;
  // clear data
  slave_address_ = 0;
  receive_size_ = 0;
}
/* USER CODE END 1 */
