/*
 * pid_controller.cpp
 *
 *  Created on: Jan 19, 2023
 *      Author: Libra
 */

#include "pid_controller.h"

PidController::PidController(q31_t cycle_time) :
cycle_time_(cycle_time){}

q31_t PidController::Control(q31_t kp, q31_t q_error, q31_t ki, q31_t q_error_integral, q31_t kd, q31_t qd_error) {
  q_error_intergral_ += q_error;
}

q31_t* PidController::GetIntegralError() {
  return &q_error_intergral_;
}
