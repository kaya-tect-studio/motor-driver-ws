/*
 * task_manager.cpp
 *
 *  Created on: 2023/01/03
 *      Author: Libra
 */

#include "task_manager.h"
#include "control_data.h"
#include "motor_config.h"
#include "motor_math.h"
#include "as5600.h"

#include "adc.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "stdlib.h"
#include "stdio.h"

static Config config_;
static MotorInput input_;
static MotorOutput output_;
static MotorCommon common_;
static MotorState pre_state_;
static MotorRef pre_ref_;

void InitTask() {
  common_.ctrl_status = CtrlStatus::IDLE;
  config_.count.acd_sample_cnt = 700;
  config_.count.pwm_period_cnt = TIM_GetPWMPulseWidthMax();
  config_.count.pwm_half_period_cnt = (config_.count.pwm_period_cnt >> 1);
  config_.hardware.num_of_pole_pairs = 11;
  config_.torque.phase_advance_angle = (PI_1 / 6); // 30 deg
  config_.torque.p_gain.d = 1;

}

void MainTask() {
  // update fsm
  switch(common_.ctrl_status) {
  case CtrlStatus::IDLE:
    if (common_.status_count > 500) common_.ctrl_status = CtrlStatus::ALIMENT;
    break;
  case CtrlStatus::ALIMENT:
    if (common_.status_count > 1000) common_.ctrl_status = CtrlStatus::START_UP;
    break;
  case CtrlStatus::START_UP:
    //if (common_.status_count > 1000) common_.ctrl_status = CtrlStatus::CURRENT_FF;
    break;
  case CtrlStatus::CURRENT_FF:
    break;
  default:
    break;
  }
  // change status
  if (common_.ctrl_status != common_.pre_ctrl_status) {
    // change status
    common_.status_count = 0;
    if (common_.pre_ctrl_status == CtrlStatus::ALIMENT) {
      config_.hardware.mechnical_angle_offset = input_.mechanical_angle;
    } else if (common_.pre_ctrl_status == CtrlStatus::START_UP) {
      common_.multiturn_count = 0;
    }
  }
  // update
  common_.status_count++;
  common_.pre_ctrl_status = common_.ctrl_status;
}

void PositionUpdateTask() {
  // reset stop watch
  TIM_ResetStopWatch();

  // reset current index
  input_.single_shunt_index = 0;
  // update position via i2c
  I2C_RefTxBuffer()[0] = AS5600_RAW_ANGLE_H;
  I2C_ReceiveAfterTransmit(AS5600_ADDR, 1, 2);
}

void CurrentSampleStartTask() {
  if (input_.single_shunt_index == 0) {
    // lap time
    common_.lap_time[0] = TIM_GetStopWatchCount();
    // start adc
    ADC_StartConversion();
  } else if (input_.single_shunt_index == 1) {
    // lap time
    common_.lap_time[2] = TIM_GetStopWatchCount();
    // start adc
    ADC_StartConversion();
  }
}

void CurrentSampleCompleteTask() {
  uint16_t adc_value = ADC_GetData();
  if (input_.single_shunt_index == 0) {
    // lap time
    common_.lap_time[1] = TIM_GetStopWatchCount();
    // get current
    input_.i_binary[output_.descend_phase_id[SortElement::MIN]] = - adc_value;
  } else if (input_.single_shunt_index == 1) {
    // lap time
    common_.lap_time[3] = TIM_GetStopWatchCount();
    // get current
    input_.i_binary[output_.descend_phase_id[SortElement::MAX]] = adc_value;
  }
  // update
  input_.single_shunt_index++;
}

void CurrentCtrlTask() {
  MotorState state;
  MotorRef ref;
  // lap time
  common_.lap_time[4] = TIM_GetStopWatchCount();
  // update input
  input_.mechanical_angle = I2C_RefRxBuffer()[0] << 8 |  I2C_RefRxBuffer()[1];
  // i_min + i_middle + i_max = 0
  input_.i_binary[output_.descend_phase_id[SortElement::MIDDLE]] = - input_.i_binary[output_.descend_phase_id[SortElement::MAX]]
                                                                   - input_.i_binary[output_.descend_phase_id[SortElement::MIN]];

  /* input -> state */
  state.mechanical_angle = (input_.mechanical_angle - config_.hardware.mechnical_angle_offset) & ENCODER_MAX;
  if ((pre_state_.mechanical_angle - ENCODER_HALF) > state.mechanical_angle) {
    // turn up
    common_.multiturn_count++;
  } else if ((state.mechanical_angle - ENCODER_HALF) > pre_state_.mechanical_angle) {
    // turn down
    common_.multiturn_count--;
  }
  state.ctrl_angle = ENCODER_MAX * common_.multiturn_count + state.mechanical_angle;
  state.electrical_angle = config_.hardware.num_of_pole_pairs *  (state.mechanical_angle << 20);
  state.i_uvw.u = input_.i_binary[PhaseId::U_PHASE];
  state.i_uvw.v = input_.i_binary[PhaseId::V_PHASE];
  state.i_uvw.w = input_.i_binary[PhaseId::W_PHASE];

  /* state -> ref */
  if (common_.ctrl_status == CtrlStatus::IDLE) {
    ref.v_uvw.u = 0;
    ref.v_uvw.v = 0;
    ref.v_uvw.w = 0;
  } else if (common_.ctrl_status == CtrlStatus::ALIMENT) {
    ref.electrical_angle = 0;
    // update trigonometric function
    q31_t sin, cos;
    arm_sin_cos_q31(ref.electrical_angle, &sin, &cos);
    // FF ctrl
    ref.v_dq.d = 0;
    ref.v_dq.q = UVW_MAX;
    // vdq -> vab
    Vdq2Vab(ref.v_dq.d, ref.v_dq.q, sin, cos, &ref.v_ab.a, &ref.v_ab.b);
    // vab -> vuvw
    Vab2Vuvw(ref.v_ab.a, ref.v_ab.b, &ref.v_uvw.u, &ref.v_uvw.v, &ref.v_uvw.w);
  } else if (common_.ctrl_status == CtrlStatus::START_UP) {
    // check mechanical angle range
    ref.electrical_angle = config_.hardware.num_of_pole_pairs * Triangle(10000000 * common_.status_count);
    // update trigonometric function
    q31_t sin, cos;
    arm_sin_cos_q31(ref.electrical_angle, &sin, &cos);
    // FF ctrl
    ref.v_dq.d = 0;
    ref.v_dq.q = UVW_MAX;
    // vdq -> vab
    Vdq2Vab(ref.v_dq.d, ref.v_dq.q, sin, cos, &ref.v_ab.a, &ref.v_ab.b);
    // vab -> vuvw
    Vab2Vuvw(ref.v_ab.a, ref.v_ab.b, &ref.v_uvw.u, &ref.v_uvw.v, &ref.v_uvw.w);
  } else if (common_.ctrl_status == CtrlStatus::CURRENT_FF) {
    ref.electrical_angle = state.electrical_angle + config_.torque.phase_advance_angle;
    // update trigonometric function
    q31_t sin, cos;
    arm_sin_cos_q31(ref.electrical_angle, &sin, &cos);
    // FB ctrl
    ref.v_dq.d = 0;
    ref.v_dq.q = UVW_MAX;
    // vdq -> vab
    Vdq2Vab(ref.v_dq.d, ref.v_dq.q, sin, cos, &ref.v_ab.a, &ref.v_ab.b);
    // vab -> vuvw
    Vab2Vuvw(ref.v_ab.a, ref.v_ab.b, &ref.v_uvw.u, &ref.v_uvw.v, &ref.v_uvw.w);
  } else if (common_.ctrl_status == CtrlStatus::CURRENT_FB) {
    // update trigonometric function
    q31_t sin, cos;
    arm_sin_cos_q31(state.electrical_angle, &sin, &cos);
    // iuvw -> iab
    Iuvw2Iab(state.i_uvw.u, state.i_uvw.u, state.i_uvw.v, &state.i_ab.a, &state.i_ab.b);
    // iab -> idq
    Iab2Idq(state.i_ab.a, state.i_ab.b, sin, cos, &state.i_dq.d, &state.i_dq.q);
    // FB ctrl
    ref.i_dq.d = 0;
    //ref.i_dq.q =
    ref.v_dq.d = config_.torque.p_gain.d * (ref.i_dq.d - state.i_dq.d);
    ref.v_dq.q = config_.torque.p_gain.q * (ref.i_dq.q - state.i_dq.q);
    // vdq -> vab
    Vdq2Vab(ref.v_dq.d, ref.v_dq.q, sin, cos, &ref.v_ab.a, &ref.v_ab.b);
    // vab -> vuvw
    Vab2Vuvw(ref.v_ab.a, ref.v_ab.b, &ref.v_uvw.u, &ref.v_uvw.v, &ref.v_uvw.w);
  }

  /* ref -> output */
  output_.pulse_width_binary[PhaseId::U_PHASE] = config_.count.pwm_half_period_cnt + q32_mult(config_.count.pwm_half_period_cnt, ref.v_uvw.u);
  output_.pulse_width_binary[PhaseId::V_PHASE] = config_.count.pwm_half_period_cnt + q32_mult(config_.count.pwm_half_period_cnt, ref.v_uvw.v);
  output_.pulse_width_binary[PhaseId::W_PHASE] = config_.count.pwm_half_period_cnt + q32_mult(config_.count.pwm_half_period_cnt, ref.v_uvw.w);

  /* phase shift */
  // sort
  uint8_t i, j;
  for (i = 0; i < 3; i++) {
    for (j = i + 1; j < 3; j++) {
      if(output_.pulse_width_binary[output_.descend_phase_id[i]] < output_.pulse_width_binary[output_.descend_phase_id[j]]) {
        PhaseId tmp = output_.descend_phase_id[i];
        output_.descend_phase_id[i] = output_.descend_phase_id[j];
        output_.descend_phase_id[j] = tmp;
      }
    }
  }

  PhaseId min_id = output_.descend_phase_id[SortElement::MIN];
  PhaseId middle_id = output_.descend_phase_id[SortElement::MIDDLE];
  PhaseId max_id = output_.descend_phase_id[SortElement::MAX];
  // min(shift to left)
  int32_t min_shift = 0;
  if (output_.pulse_width_binary[middle_id] - config_.count.acd_sample_cnt > 0) {
    // enough time to sample current
    min_shift = output_.pulse_width_binary[middle_id] - config_.count.acd_sample_cnt - output_.pulse_width_binary[min_id];
    output_.is_samplable_current[min_id] = 1;
  } else {
    output_.is_samplable_current[min_id] = 0;
  }
  TIM_RefCntUpPulseWidth()[min_id] = RangeLimit(output_.pulse_width_binary[min_id] + min_shift, 0, config_.count.pwm_period_cnt);
  TIM_RefCntDownPulseWidth()[min_id] = RangeLimit(output_.pulse_width_binary[min_id] - min_shift, 0, config_.count.pwm_period_cnt);
  // max(shift to right)
  int32_t max_shift = 0;
  if ((output_.pulse_width_binary[middle_id] + config_.count.acd_sample_cnt) < config_.count.pwm_period_cnt) {
    // enough time to sample current
    max_shift = output_.pulse_width_binary[middle_id] + config_.count.acd_sample_cnt - output_.pulse_width_binary[max_id];
    output_.is_samplable_current[max_id] = 1;
  } else {
    output_.is_samplable_current[max_id] = 0;
  }
  TIM_RefCntUpPulseWidth()[max_id] = RangeLimit(output_.pulse_width_binary[max_id] + max_shift, 0, config_.count.pwm_period_cnt);
  TIM_RefCntDownPulseWidth()[max_id] = RangeLimit(output_.pulse_width_binary[max_id] - max_shift, 0, config_.count.pwm_period_cnt);
  // middle
  output_.is_samplable_current[middle_id] = 0;
  TIM_RefCntUpPulseWidth()[middle_id] = output_.pulse_width_binary[middle_id];
  TIM_RefCntDownPulseWidth()[middle_id] = output_.pulse_width_binary[middle_id];

  // lap time
  common_.lap_time[5] = TIM_GetStopWatchCount();

  // update
  input_.single_shunt_index = 0;
  common_.current_ctrl_count++;
  pre_state_ = state;
  pre_ref_ = ref;

  if (common_.current_ctrl_count % 10 == 0) {
    //int size = sprintf((char*)USART_RefTxBuffer(), "%d,%d,%d,%d,%d,%d,%d,%d,%d\r\n",
    //    output_.pulse_width_binary[min_id],output_.pulse_width_binary[middle_id],output_.pulse_width_binary[max_id],
    //    TIM_RefCntUpPulseWidth()[min_id], TIM_RefCntUpPulseWidth()[middle_id], TIM_RefCntUpPulseWidth()[max_id],
    //    TIM_RefCntDownPulseWidth()[min_id], TIM_RefCntDownPulseWidth()[middle_id], TIM_RefCntDownPulseWidth()[max_id]);
    //int size = sprintf((char*)USART_RefTxBuffer(), "%lu,%lu,%lu,%lu,%lu,%lu\r\n", common_.lap_time[0], common_.lap_time[1], common_.lap_time[2], common_.lap_time[3], common_.lap_time[4], common_.lap_time[5]);
    //int size = sprintf((char*)USART_RefTxBuffer(), "%ld,%ld,%ld\r\n", state.i_uvw.u, state.i_uvw.v, state.i_uvw.w);
    //int size = sprintf((char*)USART_RefTxBuffer(), "%d,%ld,%ld\r\n", (int)common_.current_ctrl_count, state.electrical_angle, ref.electrical_angle);
    //USART_Transmit(size);
  }

  // reset
  for (i = 0; i < 10; i++) {
    common_.lap_time[i] = 0;
  }
}
