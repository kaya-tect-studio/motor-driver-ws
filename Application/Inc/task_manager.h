/*
 * task_manager.h
 *
 *  Created on: 2023/01/03
 *      Author: Libra
 */

#ifndef INC_TASK_MANAGER_H_
#define INC_TASK_MANAGER_H_

#ifdef __cplusplus
extern "C" {
#endif

void InitTask();
void MainTask();
void PositionUpdateTask();
void CurrentSampleStartTask();
void CurrentSampleCompleteTask();
void CurrentCtrlTask();

#ifdef __cplusplus
}
#endif



#endif /* INC_TASK_MANAGER_H_ */
