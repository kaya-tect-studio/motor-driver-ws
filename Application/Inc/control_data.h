/*
 * control_data.h
 *
 *  Created on: Jan 2, 2023
 *      Author: Libra
 */

#ifndef INC_CONTROL_DATA_H_
#define INC_CONTROL_DATA_H_

#include "constants_types.h"

struct MotorInput {
  // current
  int32_t i_binary[PhaseId::NUM_PHASE];
  uint8_t single_shunt_index;
  // angle
  int32_t mechanical_angle;
};

struct MotorOutput {
  int32_t pulse_width_binary[PhaseId::NUM_PHASE];
  PhaseId descend_phase_id[SortElement::NUM_SORT_ELEMENT] = {PhaseId::U_PHASE, PhaseId::V_PHASE, PhaseId::W_PHASE};
  uint8_t is_samplable_current[SortElement::NUM_SORT_ELEMENT] = {0, 0, 0};
};

struct MotorState {
  // current state
  uvw_t i_uvw;
  ab_t i_ab;
  dq_t i_dq;
  // angle
  int32_t electrical_angle; // -180 ~ 180
  int32_t mechanical_angle; // 0 ~ 4095
  int32_t ctrl_angle;
};

struct MotorRef {
  // current reference
  dq_t i_dq;
  // voltage reference
  dq_t v_dq;
  ab_t v_ab;
  uvw_t v_uvw;
  // angle
  int32_t electrical_angle; // -180 ~ 180
  int32_t mechanical_angle; // 0 ~ 4095 12bit
};

struct MotorCommon {
  // status
  CtrlStatus ctrl_status;
  CtrlStatus pre_ctrl_status;
  // timer count
  uint32_t status_count;
  uint64_t current_ctrl_count;
  uint32_t lap_time[10];
  // control data
  int32_t multiturn_count;
  int32_t mechanical_angle_max;
  int32_t mechanical_angle_min;
  // etc
  int32_t monitor[10];
};

#endif /* INC_CONTROL_DATA_H_ */
