/*
 * motor_config.h
 *
 *  Created on: Dec 30, 2022
 *      Author: Libra
 */

#ifndef INC_MOTOR_CONFIG_H_
#define INC_MOTOR_CONFIG_H_

#include "constants_types.h"

struct MotorHardwareConfig {
  uint8_t num_of_pole_pairs;
  int32_t mechnical_angle_offset;
};

struct MotorToqueCtrlConfig {
  dq_t p_gain;
  dq_t i_gain;
  int32_t phase_advance_angle;
};

struct MotorCountConfig {
  int16_t acd_sample_cnt;
  int32_t pwm_period_cnt;
  int32_t pwm_half_period_cnt;
};

struct PostionCtrlConfig {
  int32_t p_gain;
  int32_t i_gain;
  int32_t d_gain;
};

struct Config {
  MotorHardwareConfig hardware;
  MotorToqueCtrlConfig torque;
  MotorCountConfig count;
  PostionCtrlConfig position;
};

#endif /* INC_MOTOR_CONFIG_H_ */
