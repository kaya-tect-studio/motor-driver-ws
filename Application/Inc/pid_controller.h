/*
 * pid_controller.h
 *
 *  Created on: Jan 19, 2023
 *      Author: Libra
 */

#ifndef INC_PID_CONTROLLER_H_
#define INC_PID_CONTROLLER_H_

#include "arm_math.h"

#ifdef __cplusplus
extern "C" {
#endif

class PidController {
public:
  PidController(q31_t cycle_time);
  q31_t Control(q31_t kp, q31_t q_error, q31_t ki, q31_t q_error_integral, q31_t kd, q31_t qd_error);
  q31_t* GetIntegralError();
private:
  q31_t cycle_time_;
  q31_t q_error_intergral_;
};

#ifdef __cplusplus
}
#endif

#endif /* INC_PID_CONTROLLER_H_ */
