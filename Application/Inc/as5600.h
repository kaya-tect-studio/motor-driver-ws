/*
 * as5600.h
 *
 *  Created on: 2023/01/03
 *      Author: Libra
 */

#ifndef INC_AS5600_H_
#define INC_AS5600_H_
// slave address
#define AS5600_ADDR (0x36 << 1)
// configuration registers
#define AS5600_ZMCO        0x00
#define AS5600_ZPOS_H      0x01
#define AS5600_ZPOS_L      0x02
#define AS5600_MPOS_H      0x03
#define AS5600_MPOS_L      0x04
#define AS5600_MANG_H      0x05
#define AS5600_MANG_L      0x06
#define AS5600_CONF_H      0x07
#define AS5600_CONF_L      0x08
// output registers
#define AS5600_RAW_ANGLE_H 0x0C
#define AS5600_RAW_ANGLE_L 0x0D
#define AS5600_ANGLE_H     0x0E
#define AS5600_ANGEL_L     0x0F
// status registers
#define AS5600_STATUS      0x0B
#define AS5600_AGC         0x1A
#define AS5600_MAGNITUDE_H 0x1B
#define AS5600_MAGNITUDE_L 0x1C
// burn commands
#define AS5600_BURN        0xFF

// inline function
inline static uint8_t IsDetectedMagnet(uint8_t status) {
  return (status >> 5) & 0x01;
}

inline static uint8_t IsLowMagnet(uint8_t status) {
  return (status >> 4) & 0x01;
}

inline static uint8_t IsHighMagnet(uint8_t status) {
  return (status >> 3) & 0x01;
}

#endif /* INC_AS5600_H_ */
