/*
 * constants.h
 *
 *  Created on: Jan 2, 2023
 *      Author: Libra
 */

#ifndef INC_CONSTANTS_TYPES_H_
#define INC_CONSTANTS_TYPES_H_

#include "stdint.h"

#define PWM_FREQUENCY	      10000 // 10 kHz 100 us
#define CTRL_FREQUENCY	    1000  // 1 kHz  1 ms
#define CTRL_SAMPLING_TIME  (1.0f / CTRL_FREQUENCY)
#define UVW_MAX             0x7FFFFFFF
#define UVW_MIN             0x80000000
#define S_TO_MS             1000
#define PI_1                0x7FFFFFFF
#define PI_2                (PI_1>>1)
#define SHUT_R              0.01f
#define CURRENT_AMP         3.125f
#define VOLTAGE_TO_CURRENT  32  // 1 / SHUT_R / CURRENT_AMP
#define ENCODER_MAX         0xFFF
#define ENCODER_HALF        0x7FF

enum CtrlStatus {
  IDLE,
  ALIMENT,
  START_UP,
  CURRENT_FF,
  CURRENT_FB,
  // Mainte
  POLE_PAIR
};

enum PhaseId {
  U_PHASE,
  V_PHASE,
  W_PHASE,
  NUM_PHASE
};

enum SortElement {
  MAX,
  MIDDLE,
  MIN,
  NUM_SORT_ELEMENT
};

struct uvw_t {
  int32_t u;
  int32_t v;
  int32_t w;
};

struct ab_t {
  int32_t a;
  int32_t b;
};

struct dq_t {
  int32_t d;
  int32_t q;
};

#endif /* INC_CONSTANTS_TYPES_H_ */
