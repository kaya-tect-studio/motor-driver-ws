/*
 * motor_math.h
 *
 *  Created on: Jan 3, 2023
 *      Author: Libra
 */

#ifndef INC_MOTOR_MATH_H_
#define INC_MOTOR_MATH_H_

#include "arm_math.h"

#define SQRT3_2_Q32   1859775393  // = sqrt(3) / 2 * 2^31
#define _1_SQRT3_Q32  1239850262  // = 1 / sqrt(3) * 2^31
#define _2_3_Q32      1431655765  // = (2/3)       * 2^31

static inline q31_t q32_mult(q31_t x, q31_t y) {
  return (((q63_t)x * (q63_t)y + (0b1 << 31)) >> 31);
}

static inline q31_t float2q31(float x) {
  return (q31_t)(x * (1 << 31));
}

static inline float q312float(q31_t x) {
  return (float)(x / (1 << 31));
}

/**
 * @brief inverse park translation
 * @note V_a = V_d * cos(theta) - V_q * sin(theta)
 * @note V_b = V_q * cos(theta) + V_d * sin(theta)
 */
static inline void Vdq2Vab(q31_t d, q31_t q, q31_t sin, q31_t cos, q31_t* a, q31_t* b) {
  *a = q32_mult(cos, d) - q32_mult(sin, q);
  *b = q32_mult(cos, q) + q32_mult(sin, d);
}
/**
 * @brief inverse clarke translation(method : relative)
 * @note V_u = V_a
 * @note V_v = V_a * cos(2 / 3 * pi) + V_b * sin(2 / 3 * pi)
 * @note V_w = V_a * cos(4 / 3 * pi) + V_b * sin(4 / 3 * pi)
 */
static inline void Vab2Vuvw(q31_t a, q31_t b, q31_t* u, q31_t* v, q31_t* w) {
  b = q32_mult(b, SQRT3_2_Q32);
  *u = a;
  *v = - (a >> 1) + b;
  *w = - (a >> 1) - b;
}
/**
 * @brief clarke translation(method : relative)
 * @note i_a = (i_u * cos(0) + i_v * cos(2 / 3 pi) + i_w * cos(4 / 3 pi)) * (2 / 3)
 * @note i_b = (i_u * sin(0) + i_v * sin(2 / 3 pi) + i_w * sin(4 / 3 pi)) * (2 / 3)
 */
static inline void Iuvw2Iab(q31_t u, q31_t v, q31_t w, q31_t* a, q31_t* b) {
  *a = q32_mult(u - ((v + w) >> 1), _2_3_Q32);
  *b = q32_mult(v - w, _1_SQRT3_Q32);
}
/**
 * @brief park translation
 * @note i_d = i_a * cos(theta) + i_b * sin(theta)
 * @note i_q = i_b * cos(theta) - i_a * sin(theta)
 */
static inline void Iab2Idq(q31_t a, q31_t b, q31_t sin, q31_t cos, q31_t* d, q31_t* q) {
  *d = q32_mult(cos, a) + q32_mult(sin, b);
  *q = q32_mult(cos, b) - q32_mult(sin, a);
}

static inline int32_t RangeLimit(int32_t in, int32_t min, int32_t max) {
  if (in > max) return max;
  if (in < min) return min;
  return in;
}

static inline int32_t Triangle(uint32_t t) {
  int32_t ret;
  if (t < 0x3FFFFFFF) {
    /* 0 ~ T / 4 */
    ret = t;
  } else if (t < 0xBFFFFFFD) {
    /* T / 4 ~ 3 T /4 */
    ret = 0x7FFFFFFF - t;
  } else {
    /* 3 T / 4 ~ T */
    ret = t - 0xFFFFFFFF;
  }
  return 2 * ret;
}

static inline q31_t Sin(q31_t t) {
  return arm_sin_q31(t);
}

#endif /* INC_MOTOR_MATH_H_ */
